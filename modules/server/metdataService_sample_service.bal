import ballerina/log;
import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "metdataService" on ep {

    remote function getMetdata(grpc:Caller caller, keyWords value) returns developerDetails|error? {
        log:printInfo("Invoking getDeveloperDetails service " + value.keyWords);

        var developerInfo = getDeveloperDetails(value.keyWords);
        if (developerInfo is developerDetails) {
            grpc:Error? err = caller->send(developerInfo);

            if (err is grpc:Error) {
                log:printError("Error from connector: "+err.message());
            }

            grpc:Error? result = caller->complete();
            if (result is grpc:Error) {
                log:printError("Error in sending complete notification");
            }
        }else {
            log:printError("Error in sending error notification to caller.");
        }
    }
}

function getDeveloperDetails(string keyword) returns developerDetails? {
    map<developerDetails> developerInfo = {
        "DEV01": {
            fullname: "Saima Haitembu",
            email: "saimahatembu@gmail.com",
            language: "Java,JavaScript,HTML and CSS"
        },
        "DEV02": {
            fullname: "Nelao Kandjengo",
            email: "nelaokandjengo@gmail.com",
            language: "Java, Python, HTML and CSS"
        },
        "DEV03": {
            fullname: "James Malambo",
            email: "jamesmalambo@gmail.com",
            language: "C++/C#, JavaScript, Python, CSS and HTML"
        }
    };

    return developerInfo[keyword];
}

