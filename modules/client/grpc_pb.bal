import ballerina/grpc;

public isolated client class metdataServiceClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function getMetdata(keyWords|ContextKeyWords req) returns (developerDetails|grpc:Error) {
        map<string|string[]> headers = {};
        keyWords message;
        if (req is ContextKeyWords) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("metdataService/getMetdata", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <developerDetails>result;
    }

    isolated remote function getMetdataContext(keyWords|ContextKeyWords req) returns (ContextDeveloperDetails|grpc:Error) {
        map<string|string[]> headers = {};
        keyWords message;
        if (req is ContextKeyWords) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("metdataService/getMetdata", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <developerDetails>result, headers: respHeaders};
    }
}

public client class MetdataServiceDeveloperDetailsCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendDeveloperDetails(developerDetails response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextDeveloperDetails(ContextDeveloperDetails response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextKeyWords record {|
    keyWords content;
    map<string|string[]> headers;
|};

public type ContextDeveloperDetails record {|
    developerDetails content;
    map<string|string[]> headers;
|};

public type keyWords record {|
    string keyWords = "";
|};

public type developerDetails record {|
    string fullname = "";
    string email = "";
    string language = "";
|};

const string ROOT_DESCRIPTOR = "0A0A677270632E70726F746F22600A10646576656C6F70657244657461696C73121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C121A0A086C616E677561676518032001280952086C616E677561676522260A086B6579576F726473121A0A086B6579576F72647318012001280952086B6579576F726473323C0A0E6D65746461746153657276696365122A0A0A6765744D65746461746112092E6B6579576F7264731A112E646576656C6F70657244657461696C73620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"grpc.proto": "0A0A677270632E70726F746F22600A10646576656C6F70657244657461696C73121A0A0866756C6C6E616D65180120012809520866756C6C6E616D6512140A05656D61696C1802200128095205656D61696C121A0A086C616E677561676518032001280952086C616E677561676522260A086B6579576F726473121A0A086B6579576F72647318012001280952086B6579576F726473323C0A0E6D65746461746153657276696365122A0A0A6765744D65746461746112092E6B6579576F7264731A112E646576656C6F70657244657461696C73620670726F746F33"};
}

