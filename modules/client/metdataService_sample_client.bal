import ballerina/log;
import ballerina/io;
import ballerina/grpc;

metdataServiceClient ep = check new ("http://localhost:9090");

public function main() {
    // developerDetails info = {
    //     keyword: "DEV01"
    // };


    var response = ep->getMetdata({
        keyWords:"DEV01"
    });

    if (response is grpc:Error) {
        log:printError("Error from connector: " + response.message());
    } else {
        io:println(response);
    }
}

