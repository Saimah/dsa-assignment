public type Learners record { stringusername;stringlastname?;stringfirstname?;stringpreferred_formats?;record { stringcourse?;stringscore?;} []past_subjects?;}[];

public type Learner record {
    string username;
    string lastname?;
    string firstname?;
    string preferred_formats?;
    record  { string course?; string score?;} [] past_subjects?;
};
