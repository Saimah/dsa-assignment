import  ballerina/http;

# Successfully retrieved student materials
public type LearnerMaterialbyusernameResponse record {
    string course?;
    record  { record  { anydata[] audio?; string[] text?;}  required?; record  { string[] video?; string[] audio?;}  suggested?;}  learning_objects?;
};

# Open API for creating and updating learner profiles
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:8080") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    # + return - Successfully retrieved learners' profiles
    remote isolated function getlearner_profile() returns Learners|error {
        string  path = string `/learner_profile`;
        Learners response = check self.clientEp-> get(path, targetType = Learners);
        return response;
    }
    #
    # + return - Successfully added learner
    remote isolated function postlearner_profile(Learner payload) returns error? {
        string  path = string `/learner_profile`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    #
    # + return - Successfully updated learner
    remote isolated function  update_learnerByusername(string username, Learner payload) returns error? {
        string  path = string `/update_learner/${username}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    #
    # + return - Successfully retrieved student materials
    remote isolated function  learner_materialByusername(string username) returns LearnerMaterialbyusernameResponse|error {
        string  path = string `/learner_material/${username}`;
        LearnerMaterialbyusernameResponse response = check self.clientEp-> get(path, targetType = LearnerMaterialbyusernameResponse);
        return response;
    }
}
