import  ballerina/http;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service  /localhost:8080  on  ep0  {
        resource  function  get  learner_profile()  returns  learners {
    }
        resource  function  post  learner_profile(@http:Payload  {} learner  payload)  returns  http:Created {
    }
        resource  function  put  update_learner/[string  username](@http:Payload  {} learner  payload)  returns  http:Created {
    }
        resource  function  get  learner_material/[string  username]()  returns  record  {|*http:Created; record  {|string  course; record {}  learning_objects;|}  body;|} {
    }
}
